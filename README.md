# module terraform : BACKEND AZURE

## Définition : Backend

Un backend définit l’emplacement où Terraform stocke ses fichiers de données d’état
Terraform utilise des données d’état persistantes pour suivre les ressources qu’il gère

Ces données d’état sont stockées dans un fichier nommé `terraform.tfstate`

Un backend pour stocker l’état à distance permet à plusieurs personnes d’accéder aux données de l’État et de travailler ensemble sur cet ensemble de ressources d’infrastructure.

Pour plus d'information : [Backend](https://www.terraform.io/docs/backends/index.html)

##  Le fichier tfstate

Lors de la création d'infrastructures avec Terraform, le fichier `terraform.tfstate` est **essentiel**, 
c’est lui qui **garde à jour l’état de votre déploiement**.

Il s’agit d’un fichier texte qui décrit toute l’infrastructure.

A chaque fois que vous appliquez un code Terraform qui modifie un élément, le tfstate évolue en conséquence.

La cohérence entre le code Terraform et ce qui est déployé dans votre infrastructure est assurée par le tfstate.

## le fonctionnement 
| Etape | Action                 | Etat                                         |
|-------|------------------------|----------------------------------------------|
| 1     | Création de code Terraform | Pas de tfstate                               |
| 2     | terraform init        | Pas de tfstate                               |
| 3     | terraform plan        | Pas de tfstate                               |
| 4     | terraform apply       | tfstate qui enregistre l’état                |
| 5     | Evolution du code Terraform | Le tfstate ne bouge pas                     |
| 6     | terraform plan        | Le tfstate ne bouge pas                     |
| 7     | terraform apply       | Nouvelle version du tfstate, le n-1 est sauvegardé dans tfstate.backup |

## Restauration du backup
Le tableau précédent montre que chaque `terraform apply` crée un backup de l’ancien puis une nouvelle version du tfstate.

En cas de problème avec le fichier tfstate, on peut repartir de la version n-1 en restaurant manuellement le backup

Restaurer le n-1 c'est bien mais avoir un historique des versions c'est mieux.

C'est pourquoi il est préférable de stocker le tfstate dans un système distant qui permettra de :

- Travailler à plusieurs sur le même code Terraform
- Avoir un historique des versions du tfstate (pour pouvoir revenir à une version antérieure en cas de problème)
- chiffrer les données d'état pour les protéger les fuites de données
- Avoir une sauvegarde des données d'état en cas de perte du fichier local

# description : Backend Azure

Ce module permet de **stocker le tfstate dans un conteneur Azure Storage**.
On utilise terraform pour créer le conteneur et le compte de stockage.

## Prérequis

- Avoir installé [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- Avoir un compte [Azure](https://portal.azure.com/)
- Avoir installé [Azure CLI](https://docs.microsoft.com/fr-fr/cli/azure/install-azure-cli)
- Avoir créer un groupe de ressources ```az group create --name "nom du rg" --location "region"```

## Utilisation

Créer un fichier module_backend.tf et ajouter le code suivant :

```terraform
module "backend" {
  source = "git::git@gitlab.com:simplon-project/modules/backend.git?ref=v1.0.4"
  resource_group_name       = "nom_de_votre_rg"
  storage_account_name      = "nom_de_votre_storage_account"
  container_name            = "nom_de_votre_container"
  project                   = "nom_de_votre_projet"
  owner                     = "nom_de_votre_nom"
  allowed_ips               = ["votre_ip_publique"]
}
```
Puis exécuter les commandes suivantes :

```terraform
terraform init
terraform fmt
terraform validate
terraform plan
terraform apply
```

Ce module créer un fichier backend.tf qui contient les informations pour stocker le tfstate dans le backend Azure.

```terraform
terraform {
  backend "azurerm" {
    resource_group_name   = "nom_de_votre_rg"
    storage_account_name  = "nom_de_votre_storage_account"
    container_name        = "nom_de_votre_container"
    key                   = "terraform.tfstate"
  }
}
```
Copier ce fichier dans le répertoire où se trouve votre code Terraform.
Pour finir `terraform init` pour initialiser le backend distant et `terraform apply` pour appliquer les changements.

## Conclusion

Désormais le tfstate est stocké dans un conteneur Azure Storage.
Ce qui permet d'avoir une sauvegarde des données d'état en cas de perte du fichier local et collaborer à plusieurs sur le même code Terraform.

## Auteur

- [Kingston](https://gitlab.com/Kingston-run)