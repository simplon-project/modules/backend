# variables.tf

variable "subscription_id" {
  description = "ID de l'abonnement Azure."
  type        = string
  default     = ""
}

variable "resource_group_name" {
  description = "le nom du groupe de ressources"
  type        = string
}

variable "storage_account_name" {
  description = "Le nom du compte de stockage."
  type        = string

  validation {
    condition     = length(var.storage_account_name) >= 1 && length(var.storage_account_name) <= 24 && var.storage_account_name == lower(var.storage_account_name)
    error_message = "Le nom du compte de stockage doit avoir entre 1 et 24 caractères et être en minuscules."
  }
}

variable "container_name" {
  description = "Le nom du conteneur de stockage."
  type        = string
  default     = "tfstate"
}

variable "container_access_type" {
  description = "Le type d'accès au conteneur de stockage."
  type        = string
  default     = "private"
}

variable "account_tier" {
  description = "Le niveau de performance du compte de stockage."
  type        = string
  default     = "Standard"
}

variable "min_tls_version" {
  description = "La version minimale de TLS pour le compte de stockage."
  type        = string
  default     = "TLS1_2"
}

variable "account_replication_type" {
  description = "Le type de réplication du compte de stockage."
  type        = string
  default     = "LRS"
}

variable "project" {
  description = "Le nom du projet."
  type        = string
  default     = ""
}

variable "owner" {
  description = "Le nom du propriétaire."
  type        = string
  default     = ""
}

variable "default_action" {
  description = "Action par défaut pour les règles de pare-feu."
  type        = string
  default     = "Deny"
}

variable "allowed_ips" {
  description = "Liste des adresses IP autorisées à accéder au compte de stockage."
  type        = list(string)
  default     = []
}

variable "virtual_network_subnet_ids" {
  description = "Liste des identifiants des sous-réseaux de réseau virtuel autorisés à accéder au compte de stockage."
  type        = list(string)
  default     = []
}

