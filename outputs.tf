output "resource_group_name" {
  value = data.azurerm_resource_group.main.name
}

output "location" {
  value = data.azurerm_resource_group.main.location
}

output "storage_account_name" {
  value = azurerm_storage_account.tfstate.name
}

output "storage_container_name" {
  value = azurerm_storage_container.tfstate.name
}

output "storage_account_key" {
  value     = data.azurerm_storage_account.tfstate.primary_access_key
  sensitive = true
}

output "timestamp_formatted" {
  value = local.timestamp_formatted
}