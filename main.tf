locals {
  timestamp           = timestamp()
  day                 = formatdate("DD", local.timestamp)
  month               = formatdate("MMM", local.timestamp)
  year                = formatdate("YY", local.timestamp)
  time                = replace(formatdate("HH:mm", local.timestamp), ":", "h")
  timestamp_formatted = "${local.day}${lower(local.month)}${local.year}${local.time}"

  tags = {
    project  = var.project
    owner    = var.owner
    DeployID = local.timestamp_formatted
  }
}

data "azurerm_resource_group" "main" {
  name = var.resource_group_name
}

resource "azurerm_storage_account" "tfstate" {
  name                     = "${lower(replace(var.storage_account_name, "_", ""))}${local.timestamp_formatted}"
  resource_group_name      = data.azurerm_resource_group.main.name
  location                 = data.azurerm_resource_group.main.location
  account_tier             = var.account_tier
  account_replication_type = var.account_replication_type
  min_tls_version          = var.min_tls_version

  network_rules {
    default_action             = var.default_action
    ip_rules                   = var.allowed_ips
    virtual_network_subnet_ids = var.virtual_network_subnet_ids
  }

  lifecycle {

    #prevent_destroy       = true   # Empêcher la suppression du compte de stockage, à mettre sur false pour faire un terraform destroy
    #create_before_destroy = true   # Créer le compte de stockage avant de supprimer l'ancien
    ignore_changes = [
      name,
      resource_group_name,
    ]
  }

  tags = local.tags
}

resource "azurerm_storage_container" "tfstate" {
  name                  = "${var.container_name}${local.timestamp_formatted}"
  storage_account_name  = azurerm_storage_account.tfstate.name
  container_access_type = var.container_access_type

  lifecycle {
    ignore_changes = [
      name,
      storage_account_name,
    ]
  }
}

data "azurerm_storage_account" "tfstate" {
  name                = azurerm_storage_account.tfstate.name
  resource_group_name = azurerm_storage_account.tfstate.resource_group_name
}

resource "local_file" "backend" {
  content  = <<EOF
terraform {
  backend "azurerm" {
    resource_group_name  = "${data.azurerm_resource_group.main.name}"
    storage_account_name = "${data.azurerm_storage_account.tfstate.name}"
    container_name       = "${azurerm_storage_container.tfstate.name}"
    key                  = "terraform.tfstate"
  }
}
EOF
  filename = "${path.root}/backend.tf"
}
